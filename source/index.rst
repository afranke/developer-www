GNOME Developer Documentation
=============================

Welcome to the GNOME platform! This documentation provides essential information and resources for those developing software with and for the GNOME platform. It is also a portal to other sources of GNOME developer documentation that are available elsewhere.

Content overview
----------------

The documentation on this site is organized into three main sections:

* :doc:`Platform introduction <introduction>`: an introduction to the GNOME platform. This describes the main platform components, development tools and programming languages that can be used.
* :doc:`Guidelines <guidelines>`: detailed development information, including programming and integration guidelines.
* :doc:`Tooling <tools>`: useful tools for newcomer and experienced developers
* :doc:`Tutorials <tutorials>`: shorter pages on common topics, with step-by-step instructions.
* :doc:`Specifications <specifications>`: formal description of formats, IPC interfaces, and other interoperable parts of the GNOME platform

Additional resources
--------------------

Additional external development resources for the GNOME platform include:

* `GNOME Human Interface Guidelines <https://developer.gnome.org/hig/>`__: user experience design guidelines
* `GNOME Components <https://developer.gnome.org/components/>`__: Core and Circle components
* `Discussion forums <https://discourse.gnome.org/>`__: a great place to ask for help and advice

Platform tools
--------------

The following tools are used by the GNOME platform and have their own websites and documentation:

.. list-table::
  :widths: 20 50 30
  :header-rows: 1

  * - Tool
    - Role
    - Documentation 
  * - `GNOME Builder <https://wiki.gnome.org/Apps/Builder>`__
    - GNOME platform IDE
    - `builder.readthedocs.io <https://builder.readthedocs.io/>`__
  * - `Flatpak <https://flatpak.org>`__
    - App packaging and distribution
    - `docs.flatpak.org <https://docs.flatpak.org/en/latest/>`__
  * - `Meson <https://mesonbuild.com>`__
    - Build system
    - `mesonbuild.com <https://mesonbuild.com/>`__

Contributing
------------

If you want to contribute to the GNOME developer documentation, you can find it
`on GitLab <https://gitlab.gnome.org/Teams/documentation/developer-www/>`__.

.. toctree::
   :maxdepth: 1
   :hidden:
   :caption: Contents

   introduction
   guidelines
   tools
   tutorials
   specifications
